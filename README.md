# Automated-paper-correction

End to end solution for hassle free paper correction, for universities/teachers/students

## Setup

Make sure python is installed  
If you are using conda

```bash
conda create -n <env name here>
conda install pip
```

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install required packages.

```bash
pip install -r requirements.txt
```

## Usage

```python
conda activate <env name here>
jupyter notebook
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
